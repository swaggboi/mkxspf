# mkxspf

Perl script to take a list of files/directories and build a basic XSPF
playlist

## Install dependencies

    cpanm --installdeps .

## Usage

    $ ./mkxspf.pl
    Usage: mkxspf.pl [OPTION(S)] <FILE(S)>
    
    Create an XSPF playlist file given a list of files or directories
        -t, --title         Give the playlist a title

### With a file

Start with a list of files or directories

    $ cat list.txt
    file:///home/daniel/Videos/movie1.mp4
    file:///home/daniel/Videos/movie2.mp4

Run the script and save it to a new file

    $ ./mkxspf.pl list.txt > list.xspf

You can run it against multiple list files at one time

    $ ./mkxspf.pl list1.txt list2.txt > combinedlist.xspf

The title can be customized with the `-t` option:

    ./mkxspf -t 'new list' list.txt > newlist.xspf

### With standard input (via a pipe)

If you wanted to create a list of mp4 files:

    $ find . -name "*.mp4" -print | ./mkxspf.pl
    <?xml version="1.0" encoding="UTF-8"?>
    
    <playlist version="1" xmlns="http://xspf.org/ns/0/">
        <title>gone with the schwinn</title>
        <creator>kermit the frog</creator>
        <date>2022-01-16T18:20:05-05:00</date>
        <trackList>
            <track>
                <location>./vid1.mp4</location>
            </track>
            <track>
                <location>./vid2.mp4</location>
            </track>
        </trackList>
    </playlist>
