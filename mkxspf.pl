#!/usr/bin/env perl

# Daniel Bowling <swaggboi@slackware.uk>
# Jan 2022

use v5.22;
use File::Basename;
use Getopt::Long qw{:config no_ignore_case};
use XML::XSPF;
use XML::XSPF::Track;
#use Data::Dumper; # Uncomment for debugging

GetOptions('title|t=s', \my $title);

my $cmd = basename($0);
my (@paths, $xspf, @tracks);

if ($ARGV[0]) {
    for my $file (@ARGV) {
        open(my $fh, '<', $file) || die $!;
        chomp && push(@paths, $_) while <$fh>;
    }
}
elsif (! -t STDIN) {
    chomp(@paths = <STDIN>)
}
else {
    die <<"EOF";
Usage: $cmd [OPTION(S)] <FILE(S)>

Create an XSPF playlist file given a list of files or directories
    -t, --title         Give the playlist a title
EOF
}

$xspf = XML::XSPF->new();

for my $path (@paths) {
    my $track = XML::XSPF::Track->new();

    $track->location($path);

    push(@tracks, $track);
}

$xspf->trackList(@tracks);

say $xspf->toString();
